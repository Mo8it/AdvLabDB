from .advlabdb_independent_funs import link_formatter_factory


def str_without_semester_attr_formatter(attr):
    return attr.str_without_semester()


def part_student_part_attr_formatter(attr):
    return attr.part


part_student_part_formatter = link_formatter_factory("admin_part_student", part_student_part_attr_formatter)
part_student_formatter = link_formatter_factory("admin_part_student")

student_formatter = link_formatter_factory("admin_student")

part_formatter = link_formatter_factory("admin_part", str_without_semester_attr_formatter)
part_with_semester_formatter = link_formatter_factory("admin_part")

group_formatter = link_formatter_factory("admin_group", str_without_semester_attr_formatter)

experiment_mark_formatter = link_formatter_factory("admin_experiment_mark")

program_formatter = link_formatter_factory("admin_program")

group_experiment_formatter = link_formatter_factory("admin_group_experiment")

semester_experiment_formatter = link_formatter_factory("admin_semester_experiment", str_without_semester_attr_formatter)
semester_experiment_with_semester_formatter = link_formatter_factory("admin_semester_experiment")


def appointment_attr_formatter(attr):
    return attr.date


appointment_date_formatter = link_formatter_factory("admin_appointment", appointment_attr_formatter)
appointment_formatter = link_formatter_factory("admin_appointment")

assistant_formatter = link_formatter_factory("admin_assistant")

admin_formatter = link_formatter_factory("admin_admin")

experiment_formatter = link_formatter_factory("admin_experiment")

semester_formatter = link_formatter_factory("admin_semester")

user_formatter = link_formatter_factory("admin_user")
