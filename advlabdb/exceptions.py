class ModelViewException(Exception):
    pass


class DatabaseException(Exception):
    pass


class DatabaseImportException(Exception):
    pass
