import sys
from configparser import ConfigParser
from os import environ
from pathlib import Path


def get_data_dir() -> Path:
    data_dir_env_variable = "ADVLABDB_DATA_DIR"
    data_dir = Path(environ.get(data_dir_env_variable, "dev/data")).resolve()

    if not data_dir.is_dir():
        sys.exit(
            f"""
You did not set the environment variable {data_dir_env_variable} which is the path to the directory which holds the data of AdvLabDB including the configuration.
Read the documentation for more information!
"""
        )

    return data_dir


def load_config(file_name: str, data_dir: Path):
    config = ConfigParser()

    file = data_dir / file_name

    if not file.is_file():
        sys.exit(f"{file} is missing!")

    config.read(file)

    return config


def get_secrets(data_dir: Path):
    config = load_config("secrets.ini", data_dir)

    return config["Secrets"]


def get_settings(data_dir: Path):
    config = load_config("settings.ini", data_dir)

    return config["Settings"]


def set_config(app, data_dir: Path):
    secrets = get_secrets(data_dir)
    settings = get_settings(data_dir)

    app.config["SECRET_KEY"] = secrets["SECRET_KEY"]

    # SQLALCHEMY
    db_file = data_dir / "db/advlabdb.sqlite"
    db_file.parent.mkdir(parents=True, exist_ok=True)
    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{db_file}"

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    # Flask-Admin
    app.config["FLASK_ADMIN_FLUID_LAYOUT"] = True

    # Flask-Security
    # Enable features
    app.config["SECURITY_TRACKABLE"] = True

    # Explicitly disable features
    app.config["SECURITY_CONFIRMABLE"] = False
    app.config["SECURITY_REGISTERABLE"] = False
    app.config["SECURITY_RECOVERABLE"] = False
    app.config["SECURITY_PASSWORDLESS"] = False
    app.config["SECURITY_CHANGEABLE"] = False
    app.config["SECURITY_TWO_FACTOR"] = False
    app.config["SECURITY_UNIFIED_SIGNIN"] = False

    check_email_deliverability = settings.getboolean("CHECK_EMAIL_DELIVERABILITY", True)
    app.config["SECURITY_EMAIL_VALIDATOR_ARGS"] = {
        "check_deliverability": check_email_deliverability,
    }
    app.config["SECURITY_PASSWORD_SALT"] = secrets["SECURITY_PASSWORD_SALT"]
    app.config["SECURITY_PASSWORD_LENGTH_MIN"] = settings.getint("SECURITY_PASSWORD_LENGTH_MIN", 15)
    app.config["SECURITY_POST_LOGIN_VIEW"] = "/post-login"
