from flask_wtf import FlaskForm
from wtforms.fields import DateField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Optional

from .model_dependent_funs import selection_mark_field


class AssistantGroupExperimentFormBase(FlaskForm):
    submit = SubmitField(label="Save")


def assistant_group_experiment_form_factory(current_user, group_experiment):
    class AssistantGroupExperimentForm(AssistantGroupExperimentFormBase):
        note = TextAreaField(
            "Note",
            default=group_experiment.note,
            validators=[Optional()],
        )

    appointments = group_experiment.appointments
    appointment_num = 1
    for appointment in appointments:
        description = "Yes" if appointment.special else "No"

        setattr(
            AssistantGroupExperimentForm,
            "appointment_" + str(appointment_num),
            DateField(
                str(appointment_num),
                default=appointment.date,
                validators=[DataRequired()],
                description=description,
            ),
        )
        appointment_num += 1

    experiment_marks = group_experiment.experiment_marks
    experiment_mark_num = 1
    for experiment_mark in experiment_marks:
        setattr(
            AssistantGroupExperimentForm,
            "oral_experiment_mark_" + str(experiment_mark_num),
            selection_mark_field("Oral", experiment_mark.oral_mark),
        )
        setattr(
            AssistantGroupExperimentForm,
            "protocol_experiment_mark_" + str(experiment_mark_num),
            selection_mark_field("Protocol", experiment_mark.protocol_mark),
        )
        experiment_mark_num += 1

    return (AssistantGroupExperimentForm(), appointments, experiment_marks)
