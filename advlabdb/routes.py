from flask import Blueprint, redirect, url_for
from flask_login import current_user
from flask_security.decorators import auth_required

from . import security, settings
from .model_dependent_funs import active_semester_str

bp = Blueprint("main", __name__, root_path="/", template_folder="templates")


@bp.app_context_processor
def util_processor():
    repo_url = settings["repo_url"]

    footer = f"AdvLabDB - <a href={repo_url} target='_blank' rel='noopener noreferrer'>Source code</a>"

    return {"active_semester_str": active_semester_str, "current_user": current_user, "footer": footer}


@security.login_context_processor
def login_context_processor():
    return {"message": settings.get("login_view_message")}


@bp.route("/")
def index():
    if current_user.has_role("admin"):
        endpoint_base = "admin"
    elif current_user.has_role("assistant"):
        endpoint_base = "assistant"
    else:
        return redirect(url_for("security.login"))

    url = url_for(endpoint_base + ".index")

    return redirect(url)


@bp.route("/post-login")
@auth_required()
def post_login():
    current_active_semester = current_user.active_semester

    if current_active_semester.done:
        current_user.set_last_semester_as_active()

    endpoint_base = "admin" if current_user.has_role("admin") else "assistant"

    url = url_for(endpoint_base + "_docs.index") if current_user.login_count == 1 else url_for(endpoint_base + ".index")

    return redirect(url)


@bp.route("/user-settings")
@auth_required()
def user_settings():
    role = "admin" if current_user.has_role("admin") else "assistant"

    url = url_for("main.index") + role + "/user/edit/?id=" + str(current_user.id)

    return redirect(url)
