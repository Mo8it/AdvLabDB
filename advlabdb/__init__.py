from flask_migrate import Migrate
from flask_security.datastore import SQLAlchemyUserDatastore
from flask_security.core import Security

from .config import get_settings, get_data_dir
from .models import db, User, Role

migrate = Migrate()

data_dir = get_data_dir()
settings = get_settings(data_dir)

user_datastore = SQLAlchemyUserDatastore(db, User, Role)

security = Security(datastore=user_datastore)


def create_app(create_for_server: bool = True):
    from flask import Flask

    app = Flask(__name__)

    # Config
    from .config import set_config

    set_config(app, data_dir)

    # Setup Flask-SQLAlchemy
    db.init_app(app)

    # Setup Flask-Migrate
    migrate.init_app(app, db)

    # Setup Flask-Security-Too
    security.init_app(app)

    if create_for_server:
        # Setup views
        from .adminModelViews import init_admin_model_views

        init_admin_model_views(app)

        from .assistantModelViews import init_assistant_model_views

        init_assistant_model_views(app)

        # Register blueprints
        from .routes import bp as routes_bp

        app.register_blueprint(routes_bp)

    return app
