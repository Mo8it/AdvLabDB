#Semester
label	year
WS	22

#Part
id	number	program_label
0	1	BS
1	2	BS
2	1	MS
3	2	MS

#Student
student_number	first_name	last_name	uni_email	contact_email	bachelor_thesis	bachelor_thesis_work_group	note
12233	Max	Mustermann	max@students.uni-mainz.de	maxi@protonmail.com	NULL	NULL	NULL
13344	Kevin	Bäcker	kevin@students.uni-mainz.de	kevi@protonmail.com	NULL	NULL	NULL
24455	Dora	Musterfrau	dora@students.uni-mainz.de	NULL	Acceleration on Mars	Physikum	Random note
55122	Felix	Singer	fel@students.uni-mainz.de	NULL	Detector X	Physikum2	NULL

#Group
id	number	program_label
0	1	BS
1	1	MS

#PartStudent
student_number	part_id	group_id
12233	0	0
13344	0	0
24455	2	1
55122	2	1

#Experiment
id	number	program_label
0	1	BS
1	2	BS
2	1	MS

#GroupExperiment
id	experiment_id	group_id
0	1	0
1	2	1

#Appointment
date	special	group_experiment_id	assistant_email
20.08.2022	1	0	julia@uni-mainz.de
21.08.2022	1	0	julia@uni-mainz.de
15.11.2022	0	1	soeren@uni-mainz.de
