"""
Functions not dependent on advlabdb.models.
"""

import secrets
from string import ascii_letters, digits

from flask import current_app

PASSWORD_CHARS: str = ascii_letters + digits + "!%*+=?"


def randomPassword() -> str:
    password_length = current_app.config["SECURITY_PASSWORD_LENGTH_MIN"]
    return "".join(secrets.choice(PASSWORD_CHARS) for i in range(password_length))


def reportBadAttempt(message: str) -> None:
    print("BAD ATTEMPT:", message)
