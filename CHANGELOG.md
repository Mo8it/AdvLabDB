# Changelog

## 1.0.3

- Log to stdout/stderr instead of a log file. The volume for logs isn't needed anymore.
- Updated dependencies.

## 1.0.2

### Fixes

- Fix creating a group experiment.
- Fix the error message on semester import when the new uni email doesn't match the one in the database.

## 1.0.1

### Features

- Improve some error messages.
