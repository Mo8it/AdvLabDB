#!/usr/bin/env python3

import subprocess

import click

from cli.maintain.reset_admin_password.main import _reset_admin_password
from cli.setup.generate_secrets.main import _generate_secrets
from cli.setup.init_db.main import _init_db
from cli.test.generate_test_db.main import _generate_test_db


class Manage:
    @staticmethod
    def run(command: str, **kwargs):
        return subprocess.run(command, shell=True, check=False, **kwargs)

    @staticmethod
    def box(message: str):
        click.echo()
        click.echo(click.style(f" {message} ", bg="white", fg="black"))
        click.echo()


@click.group(
    help="Command line tool to manage AdvLabDB.",
)
def cli():
    pass


@cli.group(
    short_help="Setup commands.",
    help="Commands used to setup an AdvLabDB instance.",
)
def setup():
    pass


@setup.command(
    short_help="Generate required secrets.",
    help="Generate the file secrets.ini if it does not already exist.",
)
def generate_secrets():
    _generate_secrets()


@setup.command(
    short_help="Initialize the database.",
    help="Initialize the database if it does not already exist.",
)
def init_db():
    _init_db(Manage)


@cli.group(
    short_help="Maintenance commands.",
    help="Commands used to maintain an AdvLabDB instance.",
)
def maintain():
    pass


@maintain.command(
    short_help="Reset the password of an admin.",
    help="Reset the password of a chosen active admin. A random password will be generated. If no admins are active, a chosen admin will be activated.",
)
def reset_admin_password():
    _reset_admin_password(Manage)


@cli.group(
    short_help="Test commands.",
    help="Commands used to test AdvLabDB.",
)
def test():
    pass


@test.command(
    short_help="Generate test database.",
    help="Generate a test database if no database already exists.",
)
def generate_test_db():
    _generate_test_db()


if __name__ == "__main__":
    cli()
