#!/bin/bash

# Use the settings template as default settings if the settings file doesn't already exist
SETTINGS_FILE=/volumes/data/settings.ini
if [[ ! -f "$SETTINGS_FILE" ]]; then
    cp deploy/settings_template.ini "$SETTINGS_FILE" \
        && echo "Initialized default settings at $SETTINGS_FILE"
fi

# Generate secrets if the secrets file doesn't already exist
./manage.py setup generate-secrets

# Start the server
gunicorn --bind 0.0.0.0:80 --workers 4 --log-level error "run:create_app()"
