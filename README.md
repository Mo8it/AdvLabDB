# AdvLabDB

## About

AdvLabDB is a database with a web interface for labs.

Admins have an interface to manage students, groups, experiments, appointments, marks and analyse data.

Assistants have a separate interface to set marks and schedule appointments with students.

## Getting Started

You can deploy AdvLabDB easily using Docker. Take a look at [`compose.yaml`](compose.yaml) as a starting point for usage with `docker compose`.

One volume has to be mounted at `/volumes/data` in the container.
This volume has to contain the two files `settings.ini` and `secrets.ini`:

- A template for `settings.ini` can be found at [`deploy/settings_template.ini`](deploy/settings_template.ini). This template will be automatically copied to the data volume as default settings file if a settings file does not already exist.
- The file `secrets.ini` will be generated automatically after the first start of the container if it does not exist.

After configuring a compose file, run the command `sudo docker compose run advlabdb manage.py setup init-db` to initialize the database with a guide.

After the database initialization, run `sudo docker compose up -d` to start the container.

### All inclusive

If you don't want to use your own reverse proxy, you can use [`compose.yaml`](compose.yaml) directly with Traefik as a reverse proxy:

- Create a directory `~/advlabdb` (You use another path, but then change it also in the following steps).
- Move into the directory `~/advlabdb` and clone the repository into it as `repo`: `git clone https://codeberg.org/mo8it/AdvLabDB.git ~/advlabdb/repo`
- Copy the directory [`deploy/traefik`](deploy/traefik/) into the parent directory `~/advlabdb`: `cp ~/advlabdb/repo/deploy/traefik ~/advlabdb`
- Open the file `~/advlabdb/traefik/etc/traefik.yaml`, uncomment `email: EMAIL` and replace `EMAIL` it with your email address. You will receive a notification to this email address if any problem with your SSL certificate (for https) occur, for example certificate expiration.
- Open the file `~/advlabdb/traefik/etc/dynamic`, uncomment ``rule: Host(`SERVER_NAME`)`` and replace `SERVER_NAME` with your domain, `advlabdb.mo8it.com` for example.
- Change the time zone `TZ` in `~/advlabdb/traefik/compose.yaml` if it is not `Europe/Berlin`. Use the command `tzselect` to find out your time zone.
- Now start Traefik by running the command `sudo docker compose up -d` in the directory `~/advlabdb/traefik`.
- Create a directory for the data volume: `mkdir ~/advlabdb/data`
- Change the time zone `TZ` in `~/advlabdb/repo/compose.yaml` if it is not `Europe/Berlin`.
- Initialize the database by running `sudo docker compose run advlabdb manage.py setup init-db` in the directory `~/advlabdb/repo` and following the guide.
- Start AdvLabDB by running the command `sudo docker compose up -d` in the directory `~/advlabdb/repo`.
- Visit the domain that you specified in the Traefik configuration as `SERVER_NAME` to test if AdvLabDB is running. If this is not the case, open an issue, maybe I could help :)

## What does the name mean?

**Adv**anced **Lab** **D**ata**B**ase
