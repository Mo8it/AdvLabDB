import secrets

import click

from advlabdb import data_dir


def _generate_secrets():
    file = data_dir / "secrets.ini"

    if file.is_file():
        click.echo(f"Skipping secrets generation because the secrets file already exists at {file}.")
        return

    with file.open("w") as f:
        f.write("[Secrets]\n")

        key = secrets.SystemRandom().getrandbits(128)
        f.write(f"SECRET_KEY = {key}\n")

        salt = secrets.token_hex()
        f.write(f"SECURITY_PASSWORD_SALT = {salt}\n")
